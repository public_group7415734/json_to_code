import json
from out.root import Root

if __name__ == '__main__':
  with open('digitaltwin-input.json', 'r') as f:
    content = f.read()

  js = json.loads(content)
  root = Root.from_dict(js)
  print(root)