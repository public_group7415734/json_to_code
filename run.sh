#!/bin/bash
echo "GENERATING CODE..."
python3 json_to_code digitaltwin-input.json out

echo ""

echo "TEST THE GENERATED CODE..."
python3 test.py