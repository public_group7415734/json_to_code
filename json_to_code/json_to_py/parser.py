import json
from collections import defaultdict

from .util import Util
from .schema import Schema


class Parser:
  
  @staticmethod
  def parse_file(path):
    with open(path, 'r') as f:
      content = f.read()
    root = json.loads(content)
    return Parser.parse_dict(root)

  @staticmethod
  def parse_dict(d):
    return Schema(Parser.traverse('root', d))
    
  @staticmethod
  def traverse(node_name, node):

    classes = defaultdict(dict)
    class_name = Util.camel_to_snake_case(node_name) # ensure snake case

    for k, v in node.items():

      k = Util.camel_to_snake_case(k) # ensure snake case

      type_ = type(v)
      if v is None:
        classes[class_name].update({k: 'Any'})
      elif type_ in [int, str, float]:
        classes[class_name].update({k: type_.__name__})
      elif type_ == list and v:
        classes[class_name].update(Parser.traverse('list|' + k[:-1], v[0]))  # drop last 's'
      elif type_ == dict:
        classes[class_name].update(Parser.traverse(k, v))

    return classes
