import re

class Util:

  pattern = re.compile(r'(?<!^)(?=[A-Z])')

  @staticmethod
  def snake_to_camel_case(s):
    tokens = [t[0].upper() + t[1:] for t in s.split('_')]
    return ''.join(tokens)

  @staticmethod
  def camel_to_snake_case(s):
    return Util.pattern.sub('_', s).lower()
    