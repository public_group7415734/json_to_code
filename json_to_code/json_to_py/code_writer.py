
class CodeWriter:
  TAB_SIZE = 2

  def __init__(self, path):
    self.path = path
    self.file = None
    self.cur_tab = 0

  def __enter__(self):
      self.file = open(self.path, 'w')
      return self

  def __exit__(self, *args):
      self.file.close()

  def tab(self):
    self.cur_tab += 1

  def untab(self):
    self.cur_tab = max(self.cur_tab - 1, 0)

  def new_line(self):
     self.file.write('\n')

  def write(self, s):
    s = [ss.strip() for ss in s.split('\n')]
    s = [ss for ss in s if ss]
    for ss in s:
      self.file.write(' ' * (CodeWriter.TAB_SIZE * self.cur_tab) + ss + '\n')
