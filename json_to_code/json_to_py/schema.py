import os

from .util import Util
from .code_writer import CodeWriter

class Schema:

  DEFAULT_IMPORT = '''
    from typing import Any, List
    from dataclasses import dataclass
  '''

  CLASS_HEADER = '''
    @dataclass
    class '''
  
  FN_HEADER = '''
    @staticmethod
    def from_dict(node):
  '''

  def __init__(self, root):
    self.root = root
  
  def write(self, out_path):
    return self._write('root', self.root['root'], out_path)

  def _write(self, node_name, node, out_path):

    # depth first: write the inner class first
    for var_name, type_ in node.items():
      if isinstance(type_, dict):
        self._write(var_name, type_, out_path)

    # get file name
    _, node_name = self._remove_list_from_var_name(node_name)
    file_name = node_name + '.py'
    file_path = os.path.join(out_path, file_name)

    with CodeWriter(file_path) as f:

      # import
      f.write(Schema.DEFAULT_IMPORT)
      f.new_line()

      for var_name, type_ in node.items():
        if not isinstance(type_, dict):
          continue
        _, var_name = self._remove_list_from_var_name(var_name)
        class_name = Util.snake_to_camel_case(var_name)
        f.write(f'from .{var_name} import {class_name}')
      f.new_line()

      # class name
      this_class_name = Util.snake_to_camel_case(node_name)
      f.write(Schema.CLASS_HEADER + this_class_name + ':')

      f.tab()
      for var_name, type_ in node.items():
        is_list, var_name = self._remove_list_from_var_name(var_name)
        class_name = Util.snake_to_camel_case(var_name)

        # type hint
        if isinstance(type_, dict):
          type_hint = class_name
        elif type_ is None:
          type_hint = 'Any'
        else:
          type_hint = type_

        if is_list:
          type_hint = self._wrap_with_list(type_hint)
        
        f.write(var_name + ': ' + type_hint)

      # from_dict
      f.new_line()
      f.write(Schema.FN_HEADER)
      
      f.tab()
      args = []
      for var_name, type_ in node.items():

        is_list, var_name = self._remove_list_from_var_name(var_name) 
        lhs = var_name + '_'
        class_name = Util.snake_to_camel_case(var_name)

        # rhs
        if is_list:
          rhs = '[' + class_name + '.from_dict(n) for n in node["' + var_name + 's"]]'
        elif isinstance(type_, dict):
          rhs = class_name + '.from_dict(node["' + var_name + '"])'
        else:
          rhs = 'node.get("' + var_name + '", None)'

        args.append(lhs)
        f.write(lhs + ' = ' + rhs)
      f.write('return ' + this_class_name + '(' + ', '.join(args) + ')')

    print('WROTE:', file_path)


  def _remove_list_from_var_name(self, var_name):
      is_list = var_name.startswith('list|')
      if is_list:
        var_name = var_name[5:] # remove list|
      return is_list, var_name
  
  def _wrap_with_list(self, x):
    return 'List[' + x + ']'