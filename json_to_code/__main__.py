import os
import shutil
import sys

from json_to_py.parser import Parser


if __name__ == '__main__':
  if len(sys.argv) != 3:
    print('EXAMPLE OF USAGE:')
    print('python3 json_to_code digitaltwin-input.json out')
    exit(0)

  path, out_path = sys.argv[1:3]
  schema = Parser.parse_file(path)
  print(f"{schema.root = }")

  # ensure out folder
  try:
    shutil.rmtree(out_path)
  except:
    pass
  os.mkdir(out_path)

  # copy static files to out
  shutil.copy('json_to_code/__init__.py', out_path)

  # write py to out_path
  schema.write(out_path)

  print('bye!')